<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Presenter;



/**
 * Class BasePresenter class base presenter for all other presenters.
 * @package App\Presenters
 */
abstract class BasePresenter extends Presenter
{

}
