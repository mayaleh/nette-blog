<?php
/**
 * Created by PhpStorm.
 * User: Salim Mayaleh
 * Date: 11.6.18
 * Time: 8:36
 */

namespace App\AdminModule\Components;




use App\Models\TagsManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

/**
 * Class TagsForm form component for manage tags
 * @package App\AdminModule\Components
 */
class TagsForm extends Control
{

    /** @var TagsManager auto injected model */
    protected $tagsManager;

    public function __construct(TagsManager $tagsManager)
    {
        $this->tagsManager = $tagsManager;
    }


    /**
     * Set values to from on editing
     * @param $data array data from db for editing
     * @void
     */
    public function setDefaults($data)
    {
        if ($data){
            $this['form']->setDefaults($data);
        }else{
            $this['form']->addError("Tag nebyl nalezen.");
        }
    }

    /**
     * Creating form component
     * @return Form
     */
    protected function createComponentForm()
    {
        $form = new Form;

        $form->addHidden('id');

        $form->addText('nazev', 'Název:')
            ->setRequired(TRUE)
            ->addRule(Form::MAX_LENGTH, 'Pole %label může být max. %d znaků dlouhé', 255);


        $form->addText('popis', 'Popis:')
            ->setRequired(TRUE)
            ->addRule(Form::MAX_LENGTH, 'Pole %label může být max. %d znaků dlouhé', 255);


        $form->addSubmit('save', 'Uložit');

        $form->onSuccess[] = [$this, 'submitSucceeded'];


        return $form;
    }

    /**
     * Called after form success submit
     * @param Form $form
     * @param \stdClass $values
     * @throws \Nette\Application\AbortException
     */
    public function submitSucceeded(Form $form, \stdClass $values)
    {

        $tagData = [
            'nazev' => $values['nazev'],
            'popis' => $values['popis'],
        ];

        $id = $values->id;
        if (!empty($id)){
            $tagData['id'] = $id;
        }

        try{
            $row = $this->tagsManager->saveTag($tagData);
        }catch (\Exception $e){
            $form->addError('Při ukládání došlo k chybě: ' . $e);
            return;
        }
        if ($row !== false){
            $this->presenter->flashMessage("Ukládání proběhlo úspěšně.");

            $this->presenter->redirect('Tags:default');
        }

    }


    /**
     * Rendering form to component template
     * @void
     */
    public function render()
    {
        $this->template->setFile(__DIR__ . '/TagsForm.latte');

        $this->template->render();
    }

}


/**
 * Interface ITagsFormFactory
 * @package App\AdminModule\Components
 */
interface ITagsFormFactory
{
    /** @return TagsForm */
    public function create();
}

