<?php
/**
 * Created by PhpStorm.
 * User: Salim Mayaleh
 * Date: 9.6.18
 * Time: 18:02
 */

namespace App\AdminModule\Components;


use App\Models\ArticleManager;
use App\Models\TagsManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

/**
 * Class ArticlesForm form component for manage articles
 * @package App\AdminModule\Components
 */
class ArticlesForm extends Control
{

    /** @var ArticleManager auto injected model */
    protected $articleManager;

    /** @var TagsManager auto injected model */
    protected $tagsManager;

    /**
     * ArticlesForm constructor with injected models.
     * @param ArticleManager $articleManager injected class model
     * @param TagsManager $tagsManager injected class model
     * @void
     */
    public function __construct(ArticleManager $articleManager, TagsManager $tagsManager)
    {
        $this->articleManager = $articleManager;
        $this->tagsManager = $tagsManager;
    }


    /**
     * Sets values to form for editing
     * @param $data
     * @void
     */
    public function setDefaults($data)
    {

        if ($data){

            $tags = $this->articleManager->getTagsByArticle($data);

            $formTags = array();
            foreach ($tags as $tag) {
                $formTags[] = $tag->id;
            }
            $datum = array(
                'valid_od' => $data->valid_od->format('Y-m-d'),
                'valid_do' => $data->valid_do->format('Y-m-d'),
                'tagy'     => $formTags
            );
            $this['form']->setDefaults($data);
            $this['form']->setDefaults($datum);

        }else{
            $this['form']->addError("Článek nebyl nalezen.");
        }
    }

    /**
     * Create form
     * @return Form
     */
    protected function createComponentForm()
    {
        $form = new Form;

        $form->addHidden('id');

        $form->addText('nazev', 'Název:')
            ->setRequired(TRUE)
            ->addRule(Form::MAX_LENGTH, 'Pole %label může být max. %d znaků dlouhé', 255);


        $form->addText('popis', 'Popis:')
            ->setRequired(TRUE)
            ->addRule(Form::MAX_LENGTH, 'Pole %label může být max. %d znaků dlouhé', 255);

        $form->addText('valid_od', 'Publikovat od:')
            ->setRequired(TRUE)
            ->setType('date');

        $form->addText('valid_do', 'Publikovat do:')
            ->setRequired(TRUE)
            ->setType('date');


        $form->addTextArea('anotace', 'Anotace:');
        $form->addTextArea('obsah', 'Obsah:');

        $tags = $this->tagsManager->getAllTags();

        foreach ($tags as $item) {
            $checkbox[$item->id] = $item->nazev;
        }
        if ($checkbox){
            $form->addCheckboxList("tagy", 'Tagy:', $checkbox);
        }


        $form->addSubmit('save', 'Uložit');

        $form->onSuccess[] = [$this, 'submitSucceeded'];


        return $form;
    }


    /**
     * Called after succes submit form
     * @param Form $form
     * @param \stdClass $values
     * @throws \Nette\Application\AbortException | @void
     */
    public function submitSucceeded(Form $form, \stdClass $values)
    {

        $articleData = [
            'nazev' => $values['nazev'],
            'popis' => $values['popis'],
            'valid_od' => $values['valid_od'],
            'valid_do' => $values['valid_do'],
            'obsah' => $values['obsah'],
            'anotace' => $values['anotace'],
        ];

        $tags = $values['tagy'];

        $id = $values->id;
        if (!empty($id)){
            $articleData['id'] = $id;
        }

        try{
            $row = $this->articleManager->saveArticle($articleData);

            if (count($tags) > 0 && $row->id > 0){
                $saveTags = $this->articleManager->saveArticleTags($row->id, $tags);
            }

        }catch (\Exception $e){
            $form->addError('Při ukládání došlo k chybě: ' . $e);
            return;
        }
        if ($row !== false){
            $this->presenter->flashMessage("Ukládání proběhlo úspěšně.");

            $this->presenter->redirect('Article:default');
        }

    }


    /**
     * Rendering form component to component template
     * @void
     */
    public function render()
    {
        $this->template->setFile(__DIR__ . '/ArticlesForm.latte');

        $this->template->render();
    }

}

/**
 * Interface IArticlesFormFactory
 * @package App\AdminModule\Components
 */
interface IArticlesFormFactory
{
    /** @return ArticlesForm */
    public function create();
}

