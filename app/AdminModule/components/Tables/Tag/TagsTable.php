<?php
/**
 * Created by PhpStorm.
 * User: Salim Mayaleh
 * Date: 11.6.18
 * Time: 12:31
 */

namespace App\AdminModule\Components;


use App\Models\TagsManager;
use Nette\Application\UI\Control;
use Nette\Utils\ArrayHash;


/**
 * Class TagsTable table component for view existing tags and do actions
 * @package App\AdminModule\Components
 */
class TagsTable extends Control
{
    /** @var TagsManager Instance of model class */
    protected $tagsManager;

    /**
     * TagsTable constructor.
     * @param TagsManager $tagsManager injected model class.
     */
    public function __construct(TagsManager $tagsManager)
    {
        $this->tagsManager = $tagsManager;
    }

    /**
     * Rendering component table with data to template
     * @void
     */
    public function render()
    {
        $this->template->setFile(__DIR__ . '/TagsTable.latte');
        $this->template->allTags = $this->tagsManager->getAllTags();
        $this->template->render();
    }

}


/**
 * Interface ITagsTableFactory
 * @package App\AdminModule\Components
 */
interface ITagsTableFactory
{
    /** @return TagsTable */
    public function create();
}