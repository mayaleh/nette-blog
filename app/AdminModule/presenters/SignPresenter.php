<?php
/**
 * Created by PhpStorm.
 * User: Salim Mayaleh
 * Date: 8.6.18
 * Time: 15:59
 */

namespace App\AdminModule\Presenters;


use App\Presenters\BasePresenter;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use Nette\Security\AuthenticationException;

/**
 * Class SignPresenter sign in/out presenters.
 * @package App\AdminModule\Presenters
 */
class SignPresenter extends BasePresenter
{
    /**
     * Default action, check if user is logged.
     * @throws \Nette\Application\AbortException
     * @void
     */
    public function actionIn()
    {
        if ($this->getUser()->isLoggedIn()) {
            $this->redirect('Admin:');
        }
    }

    /**
     * Destroy logged in user. Set user log out then redirect to Front module
     * @throws \Nette\Application\AbortException
     */
    public function actionOut()
    {
        $this->getUser()->logout();
        $this->flashMessage('Odhlášení bylo úspěšné.');
        $this->redirect(':Front:Article:');
    }


    /**
     * Create Sign-in form.
     * @return Form
     */
    protected function createComponentSignInForm()
    {
        $form = new Form;
        $form->addText('username', 'Uživatelské jméno:')
            ->setRequired('Prosím vyplňte své uživatelské jméno.')
            ->setHtmlAttribute('placeholder', 'Uživatelské jméno');

        $form->addPassword('password', 'Heslo:')
            ->setRequired('Prosím vyplňte své heslo.')
            ->setHtmlAttribute('placeholder', 'Heslo');;

        $form->addSubmit('send', 'Přihlásit');

        $form->onSuccess[] = [$this, 'signInFormSucceeded'];
        $renderForm = $form->getRenderer();
        $renderForm->wrappers['form']['container'] = 'div class="login-container"';
        $renderForm->wrappers['controls']['container'] = null;
        $renderForm->wrappers['pair']['container'] = null;
        $renderForm->wrappers['label']['container'] = null;
        $renderForm->wrappers['control']['container'] = null;
        return $form;
    }


    /**
     * Called after succes submit form.
     * @param Form $form
     * @param ArrayHash $values
     * @throws \Nette\Application\AbortException
     */
    public function signInFormSucceeded(Form $form, ArrayHash $values)
    {
        try {
            $this->getUser()->login($values->username, $values->password);
            $this->redirect('Admin:');

        } catch (AuthenticationException $e) {
            $form->addError('Nesprávné přihlašovací jméno nebo heslo.');
        }
    }
}