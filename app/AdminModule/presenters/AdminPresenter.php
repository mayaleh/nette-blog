<?php
/**
 * Created by PhpStorm.
 * User: Salim Mayaleh
 * Date: 8.6.18
 * Time: 15:43
 */

namespace App\AdminModule\Presenters;


use App\Presenters\BasePresenter;

/**
 * Class AdminPresenter default module presenter and run after success sign in
 * @package App\AdminModule\Presenters
 */
class AdminPresenter extends BasePresenter
{

    protected $user;

    public function actionDefault(){
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
            return;
        }
    }
}