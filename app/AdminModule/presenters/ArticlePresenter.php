<?php
/**
 * Created by PhpStorm.
 * User: Salim Mayaleh
 * Date: 8.6.18
 * Time: 14:26
 */


namespace App\AdminModule\Presenters;

use App\Models\ArticleManager;
use App\Presenters\BasePresenter;
use Nette\Application\UI\Form;

/**
 * Class ArticlePresenter
 * @package App\AdminModule\Presenters
 */
class ArticlePresenter extends BasePresenter
{

    /** @var ArticleManager Instance of article model class to work with article repository. */
    protected $articleManager;


    /** @var \App\AdminModule\Components\IArticlesFormFactory @inject */
    public $articlesFormFactory;

    /** @var \App\Components\IArticlesDataFactory @inject */
    public $articlesTableFactory;


    private $articleEdit;

    /**
     * ArticlePresenter constructor with injected models.
     * @param ArticleManager $articleManager injected class model
     * @void
     */
    public function __construct(ArticleManager $articleManager)
    {
        parent::__construct();

        $this->articleManager = $articleManager;
    }

    /**
     * Rendering default - check user logged in
     * @void
     */
    public function renderDefault()
    {
        $this->checkUserSign();
    }

    /**
     * Delete an article.
     * @param $id int article ID
     * @throws \Nette\Application\AbortException
     * @void
     */
    public function actionDelete($id)
    {
        $this->checkUserSign();
        if ($this->articleManager->getArticleById($id)){
            $this->articleManager->removeArticle($id);
            $this->flashMessage('Článek byl úspěšně odstraněn.');
            $this->redirect('Article:default');
        }else{
            $this->flashMessage('Nastala neočekávaná chyba při odstraňování článku. Článek zřejmě už neexistuje...');
            $this->redirect('Article:default');
        }

    }

    /**
     * Get article for editing.
     * @param int $id article ID , which is editing
     * @void
     */
    public function actionEdit($id)
    {
        $this->checkUserSign();
        if ($id) ($this->articleEdit = $this->articleManager->getArticleById($id)) ? $this['articlesForm']->setDefaults($this->articleEdit) : $this->flashMessage('Článek nebyl nalezen.');
    }

    /**
     * Render article for editing.
     * @void
     */
    public function renderEdit(){
        $this->template->editing = $this->articleEdit;
        $this->template->allArticles = $this->articleManager->getAllArticles();
    }

    /**
     * For searching in repository
     * @param $id string will be searched in db
     */
    public function actionSearch($id)
    {
        $this->checkUserSign();
        if ($id)  $this['dataTable']->setDataOut($id);
    }


    /**
     * Create component form for save article
     * @return \App\AdminModule\Components\ArticlesForm
     */
    protected function createComponentArticlesForm()
    {
        $form = $this->articlesFormFactory->create();
        return $form;
    }


    /**
     * Create component data table to view existing data and manage them
     * @return \App\AdminModule\Components\ArticlesTable
     */
    protected function createComponentDataTable()
    {
        $table = $this->articlesTableFactory->create();
        $table->setTemplate("Admin");
        $table->setDataOut();
        return $table;
    }


    /**
     * Create search form
     * @return Form
     */
    protected function createComponentSearchForm()
    {
        $form = new Form;
        $form->addText('search');
        $form->addSubmit('submitSearch', 'Hledat');
        $form->onSuccess[] = [$this, 'processSearchSubmit'];
        return $form;
    }

    /**
     * Called on success search form submit. Redirect to search result
     * @param Form $form
     * @throws \Nette\Application\AbortException
     */
    public function processSearchSubmit(Form $form)
    {
        $values = $form->getValues(TRUE);
        $this->redirect('search', $values['search']);
    }

    private function checkUserSign()
    {
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
            return;
        }
    }
}