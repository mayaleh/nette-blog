<?php
/**
 * Created by PhpStorm.
 * User: Salim Mayaleh
 * Date: 9.6.18
 * Time: 16:31
 */

namespace App\AdminModule\Presenters;


use App\Models\TagsManager;
use App\Presenters\BasePresenter;

/**
 * Class TagsPresenter
 * @package App\AdminModule\Presenters
 */
class TagsPresenter extends BasePresenter
{
    /** @var TagsManager Instance of tags model class to work with tags repository. */
    private $tagsManager;


    /** @var \App\AdminModule\Components\ITagsFormFactory @inject */
    public $tagsFormFactory;

    /** @var \App\AdminModule\Components\ITagsTableFactory @inject */
    public $tagsTableFactory;

    private $tagEdit;


    /**
     * TagsPresenter constructor with injected model.
     * @param TagsManager $tagsManager
     * @void
     */
    public function __construct(TagsManager $tagsManager)
    {
        parent::__construct();
        $this->tagsManager = $tagsManager;
    }

    /**
     * Rendering default.
     * @void
     */
    public function renderDefault(){
        $this->checkUserSign();
    }


    /**
     * Delete a tag.
     * @param $id int tag ID
     * @throws \Nette\Application\AbortException
     * @void
     */
    public function actionDelete($id)
    {
        $this->checkUserSign();
        if ($this->tagsManager->getTagById($id)){
            $this->tagsManager->removeTag($id);
            $this->flashMessage('Tag byl úspěšně odstraněn.');
            $this->redirect('Tags:default');
        }else{
            $this->flashMessage('Nastala neočekávaná chyba při odstraňování tagu. Tag zřejmě už neexistuje...');
            $this->redirect('Tags:default');
        }

    }

    /**
     * Get article for editing.
     * @param int $id article ID , which is editing
     * @void
     */
    public function actionEdit($id)
    {
        $this->checkUserSign();
        if ($id) ($this->tagEdit = $this->tagsManager->getTagById($id)) ? $this['tagsForm']->setDefaults($this->tagEdit) : $this->flashMessage('Tag nebyl nalezen.');
    }

    /**
     * Rendering article for editing.
     * @void
     */
    public function renderEdit(){
        $this->template->editing = $this->tagEdit;
        $this->template->allTags = $this->tagsManager->getAllTags();
    }


    /**
     * Create From component for saving tags
     * @return \App\AdminModule\Components\TagsForm
     */
    protected function createComponentTagsForm()
    {
        $form = $this->tagsFormFactory->create();
        return $form;
    }

    /**
     * Create table of data component for displaying saved tags
     * @return \App\AdminModule\Components\TagsTable
     */
    protected function createComponentDataTable()
    {
        $table = $this->tagsTableFactory->create();
        return $table;
    }


    private function checkUserSign()
    {
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
            return;
        }
    }
}