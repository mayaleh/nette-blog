<?php
/**
 * Created by PhpStorm.
 * User: Salim Mayaleh
 * Date: 8.6.18
 * Time: 10:34
 */

namespace App\Models;


use Nette\Database\Context;
use Nette\Database\Table\IRow;
use Nette\Database\Table\Selection;
use Nette\Utils\ArrayHash;

/**
 * Class ArticleManager ensure method to manage articles in redaction.
 * @package App\Models
 */
class ArticleManager extends BaseManager
{
    /** Constants for manipulation with model. Table name and structure.*/
    const
        TABLE_NAME = 'clanky',
        COLUMN_ID = 'id',
        COLUMN_NAME = 'nazev',
        COLUMN_ANOTACE = 'anotace',
        COLUMN_POPIS = 'popis',
        COLUMN_OBSAH = 'obsah',
        COLUMN_PUB_FROM = 'valid_od',
        COLUMN_PUB_TO = 'valid_do',
        TABLE_RELATED = 'clanky_tagy',
        TABLE_RELATED_ID = 'clanky_id',
        TABLE_RELATED_COL = 'tagy_id',
        DEFAULT_ORDER = 'DESC';

    private $repositoryMain;
    private $repositoryRel;

    /**
     * ArticleManager constructor. DI database.
     * @param Context $database
     */
    public function __construct(Context $database)
    {
        parent::__construct($database);
        $this->repositoryMain = $this->database->table(self::TABLE_NAME);
        $this->repositoryRel = $this->database->table(self::TABLE_RELATED);
    }

    /**
     * Return list of articles from database.
     * @return Selection Articles list
     */
    public function getAllArticles()
    {
        return $this->repositoryMain->order(self::COLUMN_PUB_FROM . ' DESC');

    }

    public function getArticlesByValid($date)
    {
        $this->repositoryMain
            ->where('DATE_FORMAT(' . self::COLUMN_PUB_FROM . ', "%Y-%m")' . ' = ?', $date);
    }

    /**
     * Make readable only valid from current date (public)
     * Must called after get articles
     * @void
     */
    public function getOnlyPublic()
    {
        $this->repositoryMain
            ->where(self::COLUMN_PUB_FROM . ' <= ?', date("Y-m-d"))
            ->where(self::COLUMN_PUB_TO . ' >= ?', date("Y-m-d"));
    }

    /**
     * @return Selection of existing dates month in articles valid from
     */
    public function getDateValidArticles()
    {
        // SELECT DATE_FORMAT(valid_od, "%Y-%m-01") AS date FROM `clanky` GROUP BY date ORDER BY date DESC

        return $this->database->table(self::TABLE_NAME)
            ->select('DATE_FORMAT(' . self::COLUMN_PUB_FROM . ', "%Y-%m") AS date')
            ->group('date')
            ->where(self::COLUMN_PUB_FROM . ' <= ?', date("Y-m-d"));
    }

    /**
     * @param $tagId
     * @return Selection articles list
     */
    public function getArticlesByTag($tagId)
    {
        $articles = $this->repositoryMain
            ->where(
                self::COLUMN_ID,
                $this->repositoryRel
                    ->select(self::TABLE_RELATED_ID)
                    ->where(self::TABLE_RELATED_COL, $tagId)
            );
        return $articles;

    }


    /**
     * Return an article from database by the article ID
     * @param $id int article Id
     * @param bool $onlyPublic condition for select only public, default false
     * @return bool|mixed|IRow article
     */
    public function getArticleById($id, $onlyPublic = false)
    {
        if ($onlyPublic) {
            $this->getOnlyPublic();
        }
        return $this->repositoryMain
            ->where(self::COLUMN_ID, $id)
            ->fetch();
    }


    /**
     * Return list of article's tags
     * @param $article IRow row from table
     * @return bool|Selection of tags
     */
    public function getTagsByArticle($article)
    {
        return $articleTags = $article->related(self::TABLE_RELATED);
    }

    /**
     * Save an article to database. If ID is unset, it will create new article or else it will edit existing by ID
     * @param array|ArrayHash $article article
     * @return bool|int|IRow
     */
    public function saveArticle($article)
    {
        if (!isset($article[self::COLUMN_ID]))
            return $this->repositoryMain->insert($article);
        else {
            $this->repositoryMain->where(self::COLUMN_ID, $article[self::COLUMN_ID])->update($article);
            return $this->getArticleById($article[self::COLUMN_ID]);
        }
    }


    /**
     * Saving article tags
     * @param $id int id article
     * @param $tags array list of id tags
     * @return bool|int
     */
    public function saveArticleTags($id, $tags)
    {
        if ($id) {
            $this->repositoryRel->where(self::TABLE_RELATED_ID, $id)->delete();
            $numRows = 0;
            foreach ($tags as $tag) {
                $this->repositoryRel->insert([
                    self::TABLE_RELATED_ID => $id,
                    self::TABLE_RELATED_COL => $tag,
                ]);
                $numRows++;
            }
            return $numRows;
        }
        return false;
    }

    /**
     * Delete an article.
     * @param int $id the article ID
     */
    public function removeArticle($id)
    {
        $this->repositoryMain->where(self::COLUMN_ID, $id)->delete();
    }

    /**
     * @param $val string searching for
     * @return Selection
     */
    public function search($val)
    {

        $articlesData = $this->repositoryMain
            ->whereOr([
                self::COLUMN_NAME . ' LIKE ?' => '%' . $val . '%',
                self::COLUMN_ANOTACE . ' LIKE ?' => '%' . $val . '%',
                self::COLUMN_OBSAH . ' LIKE ?' => '%' . $val . '%',
                self::COLUMN_POPIS . ' LIKE ?' => '%' . $val . '%',
            ]);
        return $articlesData;
    }
}