<?php
/**
 * Created by PhpStorm.
 * User: Salim Mayaleh
 * Date: 9.6.18
 * Time: 16:18
 */

namespace App\Models;


use Nette\Database\Table\IRow;
use Nette\Database\Table\Selection;

/**
 * Class TagsManager ensure method to manage tags in redaction.
 * @package App\Models
 */
class TagsManager extends BaseManager
{
    /** Constants for manipulation with model. Table name and structure.*/
    const
        TABLE_NAME = 'tagy',
        COLUMN_ID = 'id',
        COLUMN_NAME = 'nazev',
        COLUMN_POPIS = 'popis',
        TABLE_RELATED = 'clanky_tagy',
        TABLE_RELATED_ID = 'tagy_id';


    /**
     * Return list of tags from database.
     * @return Selection list of articles
     */
    public function getAllTags()
    {
        return $this->database->table(self::TABLE_NAME)->order(self::COLUMN_ID);
    }

    /**
     * Return a tag from database by the tag ID
     * @param $id int tag Id
     * @return bool|mixed|IRow tag
     */
    public function getTagById($id){
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COLUMN_ID, $id)
            ->fetch();
    }


    //search


    /**
     * Save a tag to database. If ID is unset, it will create new article or else it will edit existing by ID
     * @param $tag
     */
    public function saveTag($tag)
    {
        if (!isset($tag[self::COLUMN_ID]))
            $this->database->table(self::TABLE_NAME)->insert($tag);
        else
            $this->database->table(self::TABLE_NAME)->where(self::COLUMN_ID, $tag[self::COLUMN_ID])->update($tag);
    }

    /**
     * Delete a tag.
     * @param string $id id
     */
    public function removeTag($id)
    {
        $this->database->table(self::TABLE_NAME)->where(self::COLUMN_ID, $id)->delete();
        $this->database->table(self::TABLE_RELATED)->where(self::TABLE_RELATED_ID, $id)->delete();
    }
}