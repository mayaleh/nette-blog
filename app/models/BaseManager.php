<?php
/**
 * Created by PhpStorm.
 * User: Salim Mayaleh
 * Date: 8.6.18
 * Time: 10:25
 */

namespace App\Models;

use Nette\Database\Context;
use Nette\Object;

/**
 * Basic model class for all models in this app.
 * Allow to access to database and work with the database.
 * @package App\Models
 */
abstract class BaseManager extends Object
{
    /** @var Context Instance class for working with database. */
    protected $database;

    /**
     * Constructor with injected class for working with the database.
     * @param Context $database auto injected class for working with database.
     */
    public function __construct(Context $database)
    {
        $this->database = $database;
    }
}