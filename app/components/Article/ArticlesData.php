<?php
/**
 * Created by PhpStorm.
 * User: Salim Mayaleh
 * Date: 11.6.18
 * Time: 9:49
 */

namespace App\Components;


use App\Models\ArticleManager;
use Nette\Application\UI\Control;

/**
 * Class ArticlesData component for view existing articles and do actions
 * @package App\Components
 */
class ArticlesData extends Control
{
    /** @var ArticleManager Instance třídy modelu pro práci s články. */
    protected $articleManager;

    private $dataOut;
    private $nadpis;
    private $temp;
    private $filtrTag;


    /**
     * ArticlesData constructor with auto injected model.
     * @param ArticleManager $articleManager
     * @void
     */
    public function __construct(ArticleManager $articleManager)
    {
        $this->articleManager = $articleManager;
    }

    /**
     * Rendering component table with data to template
     * @void
     */
    public function render()
    {
        switch ($this->temp) {
            case 'Front':
                $this->template->setFile(__DIR__ . '/ArticlesFront.latte');
                break;
            case 'Admin':
                $this->template->setFile(__DIR__ . '/ArticlesAdmin.latte');
                $this->template->nadpis = $this->nadpis;
                break;
            default:
                $this->template->setFile(__DIR__ . '/ArticlesFront.latte');
                break;
        }
        if ($this->dataOut === false) {
            $this->flashMessage('Nebyly nalazeny žádné články.', 'error');
            $this->template->render();
        } else {
            $this->template->allArticles = $this->dataOut;
            $this->template->articlesTags = $this->setDataTags();
            $this->template->render();
        }

    }


    /**
     * set variable for filtering by selected id tag
     * @param $tag int
     * @void
     */
    public function setArticlesByTag($tag)
    {
        $this->filtrTag = $tag;
    }

    /**
     * Set data for template
     * @param string $search for searching in database
     * @void
     */
    public function setDataOut($search = "")
    {
        $this->getData($search);

        switch ($this->temp) {
            case 'Front':
                $this->dataFront();
                break;
            case 'Admin':
                //$this->dataAdmin();
                break;
            default:
                $this->dataFront();
                break;
        }
    }


    /**
     * Set variable for select correct template for module
     * @param $module string module name
     * @void
     */
    public function setTemplate($module)
    {
        $this->temp = $module;
    }


    private function dataFront()
    {
        $this->articleManager->getOnlyPublic();
    }

    private function getData($search)
    {
        if ($this->filtrTag != '') {
            $this->dataOut = $this->articleManager->getArticlesByTag($this->filtrTag);
            $this->nadpis = "Články podle tagu:";
        } else {
            if ($search == "") {
                $this->dataOut = $this->articleManager->getAllArticles();
                $this->nadpis = "Všechny články:";
            } else {
                $this->dataOut = $this->articleManager->search($search);
                $this->nadpis = 'Výsledek vyhledávání "' . $search . '":';
            }
        }
    }


    private function setDataTags()
    {
        $arcTags = array();
        foreach ($this->dataOut as $item) {
            $arcTags[$item->id] = $this->articleManager->getTagsByArticle($item);
        }
        return $arcTags;
    }
}

/**
 * Interface IArticlesDataFactory
 * @package App\Components
 */
interface IArticlesDataFactory
{
    /** @return ArticlesData */
    public function create();
}