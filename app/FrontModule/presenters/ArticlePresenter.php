<?php
/**
 * Created by PhpStorm.
 * User: Salim Mayaleh
 * Date: 8.6.18
 * Time: 14:26
 */


namespace App\FrontModule\Presenters;

use App\Models\ArticleManager;
use App\Models\TagsManager;
use App\Presenters\BasePresenter;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;

/**
 * Class ArticlePresenter
 * @package App\FrontModule\Presenters
 */
class ArticlePresenter extends BasePresenter
{

    /** @var ArticleManager Instance of article model class to work with article repository. */
    private $articleManager;

    /** @var TagsManager Instance of tags model class to work with tags repository. */
    private $tagsManager;

    /** @var \App\Components\IArticlesDataFactory @inject */
    public $articlesFactory;

    private $tagFiltr;

    /**
     * ArticlePresenter constructor with injected models for ensure to access to repositories.
     * @param ArticleManager $articleManager injected model
     * @param TagsManager $tagsManager injected model
     */
    public function __construct(ArticleManager $articleManager, TagsManager $tagsManager)
    {
        parent::__construct();

        $this->articleManager = $articleManager;
        $this->tagsManager = $tagsManager;
    }

    /**
     * Rendering default. Render tags filter, date filter
     * @throws BadRequestException
     */
    public function renderDefault()
    {
        if (!($tags = $this->tagsManager->getAllTags())) throw new BadRequestException();
        $this->template->tags = $tags;
        $this->template->validFrom = $this->articleManager->getDateValidArticles();
    }

    /**
     * Rendering article detail
     * @param $id int article id
     * @throws BadRequestException if article doesn't existing or if article isn't public
     */
    public function renderDetail($id){
        if (!$id) throw new BadRequestException();
        $onlyPublic = true;
        if ($this->getUser()->isLoggedIn()) {
            $onlyPublic = false;
        }
        if (!($article = $this->articleManager->getArticleById($id, $onlyPublic))) throw new BadRequestException();
        $articleTags = $this->articleManager->getTagsByArticle($article);
        $this->template->article = $article;
        $this->template->tags = $articleTags;

    }

    /**
     * Ajax requist for filtering by selected tag.
     * @param $id int tag ID
     * @void
     */
    public function handleFiltr($id)
    {
        $this->tagFiltr = $id;
        $this->redrawControl('articlesControl');
        $this->redrawControl('flash');
    }

    /**
     * Ajax requist for filtering by selceted date
     * @param $id string date
     */
    public function handleDate($id)
    {
        $this->articleManager->getArticlesByValid($id);
        $this->redrawControl('articlesControl');
        $this->redrawControl('flash');
    }

    /**
     * Create Articles component. Public artilcles.
     * @return \App\Components\ArticlesData
     */
    protected function createComponentArticlesContent()
    {
        $content = $this->articlesFactory->create();
        $content->setTemplate('Front');

        //$content->setOnlyValid();
        $content->setArticlesByTag($this->tagFiltr);
        $content->setDataOut();
        return $content;
    }


    /**
     * Create search form component
     * @return Form
     */
    protected function createComponentSearchForm()
    {
        $form = new Form;
        $form->addText('search', 'Vyhledávání:')->setRequired(TRUE);
        $form->addSubmit('submitSearch', 'Hledat');
        $form->getElementPrototype()->addAttributes(array('class' => 'ajax'));
        $form->onSuccess[] = [$this, 'processSearchSubmit'];
        $renderer = $form->getRenderer();
        $renderer->wrappers['controls']['container'] = null;
        $renderer->wrappers['pair']['container'] = null;
        $renderer->wrappers['label']['container'] = null;
        $renderer->wrappers['control']['container'] = null;
        return $form;
    }

    /**
     * Called on success submit search form.
     * @param Form $form
     */
    public function processSearchSubmit(Form $form)
    {
        $values = $form->getValues(TRUE);

        if ($values['search'])
            $this['articlesContent']->setDataOut($values['search']);
        else
            $this->flashMessage("Nebyl nalezen žádný článek", "error");

        $this->redrawControl('articlesControl');
        $this->redrawControl('flash');
    }

}