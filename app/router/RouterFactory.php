<?php

namespace App;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;
use Nette\DI\Container;

class RouterFactory
{
	use Nette\StaticClass;

    /** @var \Nette\DI\Container */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Create router for application.
     * @return RouteList defined router app
     */
    public static function createRouter()
    {

        $router = new RouteList();


        $frontRouter = new RouteList('Front');
        $frontRouter[] = new Route('<presenter>[/<action>][/<id>]',  'Article:default');

        $adminRouter = new RouteList('Admin');
        $adminRouter[] = new Route('nette-admin/<presenter>/<action>[/<id>]', 'Admin:default');

        $router[] = $adminRouter;
        $router[] = $frontRouter;

        return $router;
    }

}
