-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Počítač: localhost
-- Vytvořeno: Stř 13. čen 2018, 11:36
-- Verze serveru: 5.7.22-0ubuntu0.16.04.1
-- Verze PHP: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `blog-nette`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `clanky`
--

CREATE TABLE `clanky` (
  `id` int(10) UNSIGNED NOT NULL,
  `nazev` varchar(260) COLLATE utf8mb4_unicode_ci NOT NULL,
  `anotace` text COLLATE utf8mb4_unicode_ci,
  `popis` varchar(260) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `obsah` longtext COLLATE utf8mb4_unicode_ci,
  `valid_od` date DEFAULT NULL,
  `valid_do` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Vypisuji data pro tabulku `clanky`
--

INSERT INTO `clanky` (`id`, `nazev`, `anotace`, `popis`, `obsah`, `valid_od`, `valid_do`) VALUES
(20, 'Edit Kovaný interiérový design a nábytek', 'Původně se termín wiki používal zcela opačně. Wiki bylo označení typu softwaru a weby postavené na wiki byly označovány jako wiki-weby. Postupně došlo k přenesení významu slova wiki na výsledný web a pro použitou platformu byl zaveden termín wiki-software.', 'popipipsoak  askj ask k kajs k', 'Původně se termín wiki používal zcela opačně. Wiki bylo označení typu softwaru a weby postavené na wiki byly označovány jako wiki-weby. Postupně došlo k přenesení významu slova wiki na výsledný web a pro použitou platformu byl zaveden termín wiki-software.', '2018-06-01', '2018-06-24'),
(23, 'e-shop', 'dsasdasdasd', 'KJnlkjn', 'asdasdasdasd', '2018-03-01', '2019-02-03'),
(24, 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime pl', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime plLorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime plLorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime pl', '2018-01-01', '2018-06-30'),
(25, 'In convallis.', 'In convallis. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Maecenas aliquet accumsan leo. Phasellus et lorem id felis nonummy placerat. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et ', 'In convallis.', 'In convallis. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Maecenas aliquet accumsan leo. Phasellus et lorem id felis nonummy placerat. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et In convallis. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Maecenas aliquet accumsan leo. Phasellus et lorem id felis nonummy placerat. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et ', '2017-07-13', '2018-06-30'),
(26, 's. Vivamus a', 'In convallis. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Maecenas aliquet accumsan leo. Phasellus et lorem id felis nonummy placerat. Sed ut perspiciatis unde omnis iste natus error sit vIn convallis. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Maecenas aliquet accumsan leo. Phasellus et lorem id felis nonummy placerat. Sed ut perspiciatis unde omnis iste natus error sit v', 's. Vivamus a', 'In convallis. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Maecenas aliquet accumsan leo. Phasellus et lorem id felis nonummy placerat. Sed ut perspiciatis unde omnis iste natus error sit vIn convallis. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Maecenas aliquet accumsan leo. Phasellus et lorem id felis nonummy placerat. Sed ut perspiciatis unde omnis iste natus error sit v', '2017-07-12', '2018-06-20'),
(27, 'Suspendisse nis', 'Suspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nis', 'Suspendisse nisSuspendisse nisSuspendisse nis', 'Suspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nisSuspendisse nis', '2018-08-31', '2018-10-19'),
(28, 'Clanek', 'Jeden tagJeden tagJeden tagJeden tagJeden tagJeden tag', 'Jeden tag', 'Jeden tagJeden tagJeden tagJeden tagJeden tagJeden tagJeden tagJeden tagJeden tagJeden tagJeden tagJeden tagJeden tagJeden tagJeden tagJeden tagJeden tagJeden tagJeden tagJeden tagJeden tagJeden tagJeden tag', '2018-06-11', '2018-06-29'),
(29, 'Salim Mayaleh', 'skjgfkjdvh', 'naše roztomilá holčička', 's.kdngv-sdzxmc.kshflesnblnvůlsamdůlsjvkldnvclk moůmsc.,sanclkzx dnvmjgofnkmhkdf', '2018-06-04', '2018-06-12'),
(30, 'Článek bez tagů', 'Článek bez tagůČlánek bez tagůČlánek bez tagů', 'Článek bez tagů', 'Článek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagůČlánek bez tagů', '2018-05-31', '2018-06-21');

-- --------------------------------------------------------

--
-- Struktura tabulky `clanky_tagy`
--

CREATE TABLE `clanky_tagy` (
  `clanky_id` int(10) UNSIGNED NOT NULL,
  `tagy_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Vypisuji data pro tabulku `clanky_tagy`
--

INSERT INTO `clanky_tagy` (`clanky_id`, `tagy_id`) VALUES
(20, 2),
(24, 2),
(26, 2),
(23, 3),
(25, 3),
(20, 4),
(24, 4),
(25, 4),
(27, 4),
(24, 5),
(20, 6),
(27, 6),
(28, 6),
(29, 7);

-- --------------------------------------------------------

--
-- Struktura tabulky `tagy`
--

CREATE TABLE `tagy` (
  `id` int(10) UNSIGNED NOT NULL,
  `nazev` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `popis` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Vypisuji data pro tabulku `tagy`
--

INSERT INTO `tagy` (`id`, `nazev`, `popis`) VALUES
(2, 'Tag', 'popis tagu'),
(3, 'Tag 2', 'lorem impsum'),
(4, 'jaskjn', 'KJnlkjn'),
(5, 'cardiolab', 'Sportovně - kardiologická laboratoř'),
(6, 'novy tag', 'Popis'),
(7, 'Salim', 'holčička');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `clanky`
--
ALTER TABLE `clanky`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `clanky_tagy`
--
ALTER TABLE `clanky_tagy`
  ADD PRIMARY KEY (`clanky_id`,`tagy_id`),
  ADD KEY `clanky_tagy_tagy` (`tagy_id`);

--
-- Klíče pro tabulku `tagy`
--
ALTER TABLE `tagy`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `clanky`
--
ALTER TABLE `clanky`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT pro tabulku `tagy`
--
ALTER TABLE `tagy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `clanky_tagy`
--
ALTER TABLE `clanky_tagy`
  ADD CONSTRAINT `clanky_tagy_clanky` FOREIGN KEY (`clanky_id`) REFERENCES `clanky` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `clanky_tagy_tagy` FOREIGN KEY (`tagy_id`) REFERENCES `tagy` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;